# LHCb ETF tests

This repository contains the LHCb ETF tests.

## Storage tests
The storage tests are separated by protocol (GSIFTP, WebDAV and Xrootd) and test different functionalities to provide a detailed view of the failures and make troubleshooting easier. They are summarised in the following table.

| Test name | Short description |
|-----------|-------------|
| [org.lhcb.SE-GSIftp-connection](docs/gsiftp-connection.md) | Checks if it is possible to open a simple connection to the endpoint |
| [org.lhcb.SE-GSIftp-write](docs/gsiftp-write.md) | Checks if it is possible to write and read back a file using GFAL2 and the GSIFTP protocol |
| [org.lhcb.SE-WebDAV-connection](docs/webdav-connection.md) | Checks if it is possible to open a simple connection to the endpoint |
| [org.lhcb.SE-WebDAV-ssl](docs/webdav-ssl.md) | Checks if it is possible to connect via SSL to the endpoint |
| [org.lhcb.SE-WebDAV-extensions](docs/webdav-extensions.md) | Checks if the endpoint supports the WebDAV extensions |
| [org.lhcb.SE-WebDAV-write](docs/webdav-write.md) | Checks if it is possible to write and read back a file using GFAL2 and the WebDAV protocol |
| [org.lhcb.SE-xrootd-connection](docs/xrootd-connection.md) | Checks if it is possible to open a simple connection to the endpoint |
| [org.lhcb.SE-xrootd-write](docs/xrootd-write.md) | Checks if it is possible to write and read back a file using GFAL2 and the Xrootd protocol |
