FROM gitlab-registry.cern.ch/etf/docker/etf-base:el9

ENV NSTREAM_ENABLED=0

# Streaming
COPY ./config/ocsp_handler.cfg /etc/nstream/

# Install gfal
RUN yum -y install python3-gfal2 python3-gfal2-util gfal2-plugin-srm gfal2-plugin-gridftp gfal2-plugin-http gfal2-plugin-sftp gfal2-plugin-xrootd xrootd-client

# ARC config
RUN mkdir /opt/omd/sites/$CHECK_MK_SITE/.arc
COPY ./config/client.conf /opt/omd/sites/$CHECK_MK_SITE/.arc/
RUN chown -R $CHECK_MK_SITE /opt/omd/sites/$CHECK_MK_SITE/.arc/

# MW env
COPY ./config/grid-env.sh /etc/profile.d/
RUN echo "source /etc/profile.d/grid-env.sh" >> /opt/omd/sites/$CHECK_MK_SITE/.profile

# CONDOR
COPY ./config/condor/90-ssl-auth /etc/condor-ce/config.d/90-ssl-auth

# # ARC
# RUN rpm -ivh https://download.nordugrid.org/packages/nordugrid-release/releases/6/centos/el7/x86_64/nordugrid-release-6-1.el7.noarch.rpm
# RUN yum -y install nordugrid-arc-client nordugrid-arc-plugins-needed nordugrid-arc-plugins-globus nordugrid-arc-plugins-arcrest

# Xroot
RUN yum -y install python3-xrootd xrootd-libs xrootd-client-libs

# ETF WN-qFM payload
RUN mkdir -p /usr/libexec/grid-monitoring/wnfm/lib/python/site-packages
RUN mkdir -p /usr/libexec/grid-monitoring/wnfm/bin
RUN cp /usr/bin/etf_wnfm /usr/libexec/grid-monitoring/wnfm/bin/
RUN cp -r /usr/lib/python3.9/site-packages/pexpect /usr/libexec/grid-monitoring/wnfm/lib/python/site-packages
RUN cp -r /usr/lib/python3.9/site-packages/ptyprocess /usr/libexec/grid-monitoring/wnfm/lib/python/site-packages
RUN cp -r /usr/lib/python3.9/site-packages/wnfm /usr/libexec/grid-monitoring/wnfm/lib/python/site-packages

COPY config/patches/vo_feed_api.patch /opt/patches/vo_feed_api.patch
RUN patch /usr/lib/python3.9/site-packages/vofeed/api.py /opt/patches/vo_feed_api.patch

# ETF local checks
COPY ./config/lhcb_vofeed.py /usr/lib/ncgx/x_plugins/
COPY ./config/wlcg_lhcb.cfg /etc/ncgx/metrics.d/
RUN mkdir -p /usr/libexec/grid-monitoring/probes/org.lhcb/wnjob

# and payload
COPY ./src/probes/WN* /usr/libexec/grid-monitoring/probes/org.lhcb/wnjob/org.lhcb/probes/
COPY ./src/probes/se* /usr/libexec/grid-monitoring/probes/org.lhcb/
COPY ./src/check_pilot_results /usr/libexec/grid-monitoring/probes/org.lhcb/

# ETF config
COPY ./config/lhcb_checks.cfg /etc/ncgx/conf.d/
COPY ./config/ncgx.cfg /etc/ncgx/

COPY ./config/add-keys.sh /opt/omd/sites/etf/.oidc-agent/

EXPOSE 80 443 6557
ENTRYPOINT ["/usr/sbin/init"]
