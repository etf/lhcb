#! /bin/env python3

import os
import calendar
import time
import socket
import ssl
import sys
import urllib.request, urllib.error
import http
import xml.etree.ElementTree
import zlib
import logging
import subprocess

import OpenSSL
import gfal2

import nap.core


WD_VERSION = '0.1'
WD_CAPATH='/etc/grid-security/certificates'

app = nap.core.Plugin(description="LHCb SAM xrootd endpoint probe")
app.add_argument("-E", "--endpoint", help="xrootd endpoint (host:port)")
app.add_argument("-P", "--port", default=1094, type=int,
                 help="xrootd port number", )
app.add_argument("-X", "--x509vo", help="location of VOMS proxy")
app.add_argument("-T", "--target", help="target path")
app.add_argument("-4", "--ipv4", help="use only IPv4 for probing",
                 action="store_true")
app.add_argument("-6", "--ipv6", help="use only IPv6 for probing",
                 action="store_true")
app.add_argument("-b", "--basic_only", help="Only perform basic tests, no write",
                         action="store_true")


def logging_setup_gfal2(debugFlag):

    global HandlerList
    HandlerList = []
    for hndlr in logging.root.handlers[:]:
        logging.root.removeHandler(hndlr)
        hndlr.flush()
        HandlerList.append( hndlr )

    if ( debugFlag == True ):
        logging.basicConfig(datefmt="      %H:%M:%S",
                            format="%(asctime)s [%(levelname).1s] %(message)s",
                            level=logging.DEBUG)
        gfal2.set_verbose(gfal2.verbose_level.debug)

    else:
        logging.basicConfig(datefmt="      %H:%M:%S",
                            format="%(asctime)s [%(levelname).1s] %(message)s",
                            level=logging.INFO)
        gfal2.set_verbose(gfal2.verbose_level.verbose)

    return

def logging_restore_etf():

    global HandlerList
    if ( HandlerList is not None ):
        for hndlr in logging.root.handlers[:]:
            logging.root.removeHandler(hndlr)
            hndlr.flush()
        for hndlr in HandlerList:
            logging.root.addHandler(hndlr)
        HandlerList = None

    return


@app.metric(seq=1, metric_name="org.lhcb.SE-xrootd-connection", passive=True)
def probe_connection(args, io):

# This metric prints some softare versions, information from the user proxy,
# finds the IP addresses of the hostname and does a DNS reverse lookup and
# finally opens a connection to the hostname on the specified port

    io.write("\n\n")

    try:
        if (args.endpoint is None):
            if ((args.hostname is None) or
                (args.hostname == '') or
                (args.hostname == 'localhost')):
                io.write("Neither xrootd endpoint nor hostname provided\n")
                io.set_status(nap.UNKNOWN, "Plugin argument error")
                return

            else:
                args.endpoint = args.hostname + ":%d" % args.port

        else:
            args.hostname = args.endpoint.split(":", 1)[0]
            args.port = int(args.endpoint.split(":", 1)[1])

        if (args.ipv4 and args.ipv6):
            io.write("IPv4/6 flags are mutually exclusive\n")
            io.set_status(nap.UNKNOWN, "Plugin argument error")
            return

        dual_stack = not args.ipv4 and not args.ipv6

        if (args.x509vo is None):
            io.write("An X.509 certificate with VOMS extension is required\n")
            io.set_status(nap.UNKNOWN, "Plugin argument error")
            return

    except Exception as ex:
        io.write("Argument verification failed: %s\n" % str(ex))
        io.set_status(nap.UNKNOWN, "Plugin argument error")
        return

    now = int(time.time())
    io.write("Starting LHCb xrootd connection test of %s on %s\n\n" %
             (args.endpoint, time.strftime("%Y-%b-%d %H:%M:%S UTC",
                                           time.gmtime(now))))
    statusFlag = nap.OK
    summaryMSG = "Endpoint reachable on all addresses"

    os.environ["XRD_APPNAME"] = "org.lhcb.SE-xrootd"
    if (args.ipv4):
        io.write("Using only IPv4 for probing\n")
        os.environ["XRD_NETWORKSTACK"] = "IPv4"

    elif (args.ipv6):
        io.write("Using only IPv6 for probing\n")
        os.environ["XRD_NETWORKSTACK"] = "IPv6"

    else:
        if ("XRD_NETWORKSTACK" in os.environ):
            del os.environ["XRD_NETWORKSTACK"]
        io.write("Using both IPv4 and IPv6 for probing\n")

    io.write("\nSE-xrootd probe version: %s\n" % WD_VERSION)
    io.write("Python version: %d.%d.%d\n" % sys.version_info[0:3])
    io.write("gfal2 version: %s\n" % gfal2.get_version())
    io.write("python3-gfal2 version: %s\n\n" % gfal2.__version__)
    io.write("xrootd version: ")
    cmd = ["/usr/bin/xrdfs", args.endpoint, "query", "config", "version"]
    try:
        rc, outerr = nap.core.sub_process(cmd, timeout=60)
        io.write(outerr.decode() + '\n')

    except subprocess.TimeoutExpired:
        io.write("version query timed out\n")

    try:
        with open(args.x509vo, "rb") as fd:
            cert = fd.read()
            x509 = OpenSSL.crypto.load_certificate(OpenSSL.crypto.FILETYPE_PEM,
                                                   cert)
            subject = "".join(["/%s=%s" % (c[0].decode(), c[1].decode()) \
                               for c in x509.get_subject().get_components()])
            io.write("Certificate subject: %s\n" % subject)
            dates = {}
            dates['since'], dates['until'] = (
                    time.strptime(
                            getattr(x509, x)().decode('ascii'),
                            '%Y%m%d%H%M%SZ'
                        )
                    for x in ('get_notBefore', 'get_notAfter')
                )
            for key, val in dates.items():
                io.write(
                    "Proxy valid %s: %s UTC\n" % (
                            key,
                            time.strftime("%Y-%m-%d %H:%M:%S", val)
                        )
                    )

            if (int(calendar.timegm(dates['until'])) < now + 300):
                io.write("Expired X.509 certificate!\n")
                io.set_status(nap.UNKNOWN, "Expired X.509 certificate")
                return
            elif (int(calendar.timegm(dates['since'])) > now + 10):
                io.write("X.509 certificate is not valid yet!\n")
                io.set_status(nap.UNKNOWN, "X.509 certificate is not valid yet")
                return

            # Check VOMS extensions
            voms = False
            for i in range(x509.get_extension_count()):
                if (x509.get_extension(i).get_short_name() == b"UNDEF"):
                    extb = x509.get_extension(i).get_data()
                    if (extb.find(b"/lhcb/") >=0):
                        io.write("LHCb VOMS extension found\n")
                        voms = True
                        break

            if (not voms):
                io.write("LHCb VOMS extension not found\n")
                statusFlag = nap.WARNING
                summaryMSG = "X.509 certificate without LHCb VOMS extension"

        del voms, dates, subject, x509, cert

    except Exception as ex:
        io.write("X.509 certificate decoding failed: %s\n" % str(ex))
        statusFlag = nap.WARNING
        summaryMSG = "X.509 certificate load/crypto error"

# Looking for the IP addresses
    iplist = []
    try:
        if (not args.ipv6):
            iplist.extend(socket.getaddrinfo(args.hostname, args.port,
                                             socket.AF_INET,
                                             socket.SOCK_STREAM))
    except Exception as ex:
        io.write("No IPv4 address found: %s\n" % str(ex))
        
    try:
        if (not args.ipv4):
            iplist.extend(socket.getaddrinfo(args.hostname, args.port,
                                             socket.AF_INET6,
                                             socket.SOCK_STREAM))
    except Exception as ex:
        io.write("No IPv6 address found: %s\n" % str(ex))        

    if (len(iplist) == 0):
        io.write("No IP address translation for %s\n" % args.hostname)
        io.set_status(nap.CRITICAL, "IP address lookup error")
        return

    args.address = [e[4][0] for e in iplist]

    io.write("\nChecking reverse DNS:\n")
    io.write("\nHost %s translates to %d addresses: %s\n" %
            (args.hostname, len(iplist), ", ".join(args.address)))
    for ip in args.address:
        try:
            node = socket.gethostbyaddr(ip)[0]
            io.write("Address %s has name %s\n" % (ip, node))

        except socket.herror:
            io.write("No IP name associated with address %s\n" % ip)
            statusFlag = nap.WARNING
            summaryMSG = "Reverse DNS lookup error"

        except Exception as ex:
            io.write("IP name lookup for %s failed: %s\n" % (ip, str(ex)))
            statusFlag = nap.WARNING
            summaryMSG = "Reverse DNS lookup error"

    io.write("\nChecking connections:\n")

    try:
        for ip in sorted(args.address):
            if (ip.count(".") == 3):
                sckt = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            elif (ip.count(":") >= 2):
                sckt = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
            else:
                args.address.remove(ip)
                continue

            sckt.settimeout(30.0)
            try:
                sckt.connect((ip, args.port))

            except socket.timeout:
                io.write("Connection attempt to %s timed out\n" % ip)
                args.address.remove(ip)
                statusFlag = nap.WARNING
                summaryMSG = "Connection timeout"
                continue

            except Exception as ex:
                io.write("Connection attempt to %s failed: %s\n" % (ip,
                                                                  str(ex)))
                args.address.remove(ip)
                statusFlag = nap.WARNING
                summaryMSG = "Connection error"
                continue

            io.write("Connected to port %d of %s\n" % (args.port, ip))
            sckt.shutdown(socket.SHUT_RDWR)
            sckt.close()

    except Exception as ex:
        io.write("Connection check failed: %s\n" % str(ex))
        statusFlag = nap.WARNING
        summaryMSG = "Connection error"

    ipv4Flag = False
    ipv6Flag = False
    for ip in args.address:
        if ( ip.count(".") == 3 ):
            ipv4Flag = True

        elif ( ip.count(":") >= 2 ):
            ipv6Flag = True

    if (not ipv4Flag and args.ipv4):
        io.write("Endpoint does not have a reachable IPv4 IP address\n")
        io.set_status(nap.CRITICAL, "Endpoint does not provide IPv4 access")
        return

    if (not ipv6Flag and args.ipv6):
        io.write("Endpoint does not have a reachable IPv6 IP address\n")
        io.set_status(nap.CRITICAL, "Endpoint does not provide IPv6 access")
        return

    if (dual_stack and (not ipv4Flag or not ipv6Flag)):
        io.write("Endpoint does not have reachable IPv4 and v6 addresses\n")
        statusFlag = nap.WARNING
        summaryMSG = "Cannot access endpoint via both IPv4 and IPv6 addresses"

    if (len(args.address) == 0):
        io.write("Connection check failed\n")
        statusFlag = nap.CRITICAL
        summaryMSG = "Connection check error"

    io.set_status(statusFlag, summaryMSG)
    return

########################################################################

@app.metric(seq=2, metric_name="org.lhcb.SE-xrootd-write",
            passive=True)
def probe_write(args, io):
    if args.basic_only:
        io.set_status(nap.OK, "Requested to skip write check")
        return

    for probe in app.sequence:
        try:
            if (( probe[1] == "org.lhcb.SE-xrootd-connection" )):
                for result in app.metric_results():
                    if ( probe[0].__name__ == result[0] ):
                        if (( result[1] == nap.CRITICAL ) or 
                            ( result[1] == nap.UNKNOWN )):
                            io.set_status(nap.UNKNOWN, "Skipping write check")
                            return
        except IndexError:
            pass

    now = time.time()
    io.write("\n\n")
    io.write("Starting LHCb xrootd write test of %s on %s\n" %
             (args.endpoint, time.strftime("%Y-%b-%d %H:%M:%S UTC",
                                       time.gmtime(now))))
    statusFlag = nap.OK
    summaryMSG = "File write test successful"

    path = args.target
    os.environ['XrdSecPROTOCOL'] = "gsi,unix"
    os.environ["X509_USER_PROXY"] = args.x509vo

    cntxt = gfal2.creat_context()
    cntxt.set_user_agent("org.lhcb.SE-xrootd", WD_VERSION)

    params = cntxt.transfer_parameters()
    params.timeout = 90
    params.overwrite = True

    logging_setup_gfal2(args.debug)

    io.write("\nChecking file write to endpoint %s\n" % args.endpoint)
    proto = "root://"
    filename = "se_xrootd_" \
               + time.strftime("%Y%m%d_%H%M%S_", time.gmtime(time.time())) \
               + socket.gethostname().split(".")[0] + ".wrt"
    if (path[-1] == "/"):
        dir = proto + args.endpoint + '/' + path + "ETFTest"
    else:
        dir = proto + args.endpoint + '/' + path + "/ETFTest"
    fileURI = dir + "/" + filename
    fd = None
    try:
        io.write("Test file: %s\n" % filename)
        buffer = (("SAM xrootd write test\n%s\n" % fileURI ) + "="*256 )[:256]
        fd = cntxt.open(fileURI, "w")
        nb = fd.write(buffer)
        fd = None
        if (nb != len(buffer)):
            io.write("Partial test file write: ")
            statusFlag = nap.CRITICAL
            summaryMSG = "Partial file write"
        io.write("%d characters written for a total of %d\n" %
                 (nb, len(buffer)))
        stat = cntxt.stat(fileURI)
        if (stat.st_size != len(buffer)):
            io.write("Write test file size mismatch!\n")
            statusFlag = nap.CRITICAL
            summaryMSG = "Write file size mismatch"
        else:
            io.write("Write test file size correct\n")

    except gfal2.GError as ex:
        io.write("Write check of %s at %s failed: %s\n" % (fileURI,
                                                   args.endpoint, str(ex)))
        statusFlag = nap.CRITICAL
        summaryMSG = "GFAL2 write check error"
        io.set_status(statusFlag, summaryMSG)
        return

    finally:
        del fd

    chcksum1 = "%8.8x" % zlib.adler32( buffer.encode('utf-8') )
    try:
        chcksum2 = cntxt.checksum(fileURI, "ADLER32")
        if (chcksum2 == chcksum1):
            io.write("Checksum via gfal2 is correct\n")
        else:
            io.write("Checksum via gfal2 is wrong!\n")
            statusFlag = nap.CRITICAL
            summaryMSG = "new file checksum mismatch"

    except gfal2.GError as ex:
        io.write("New file checksum check of %s at %s failed: %s\n" % \
                                        (filename, args.endpoint, str(ex)))
        statusFlag = nap.CRITICAL
        summaryMSG = "GFAL2 new file checksum check error"

    io.write("\nChecking file removal at endpoint %s\n" % args.endpoint)
    try:
        rc = cntxt.unlink(fileURI)
        if (rc == 0):
            io.write("File removed successfully\n")
        else:
            io.write("Write test file removal failed with rc=%d!\n" % rc)
            if (statusFlag < nap.WARNING):
                statusFlag = nap.WARNING
                summaryMSG = "Write file removal error"
    except gfal2.GError as ex:
        io.write("Removal check of %s failed: %s\n" % (filename, str(ex)))
        if (statusFlag < nap.WARNING):
            statusFlag = nap.WARNING
            summaryMSG = "GFAL2 file removal check error"

    logging_restore_etf()

    io.set_status(statusFlag, summaryMSG)
    return

########################################################################

@app.metric(seq=9, metric_name="org.lhcb.SE-xrootd-summary", passive=False)
def probe_summary(args, io):
    io.write("\n\n")

    def statusOrder(status):
        statusFlags = [ nap.OK, nap.WARNING, nap.UNKNOWN, nap.CRITICAL ]
        try:
            return statusFlags.index(status)

        except ValueError:
            return statusFlags.index(nap.UNKNOWN)

    def statusString(status):
        if ( status == nap.OK ):
            return "Ok"

        elif ( status == nap.WARNING ):
            return "Warning"

        elif ( status == nap.CRITICAL ):
            return "Error"

        else:
            return "Unknown"

    statusFlag = nap.OK
    summaryMSG = "xrootd test successful"

    for result in app.metric_results():
        try:
            io.write("%s: %s\n" % (result[0], statusString(result[1])))
            if (statusOrder(result[1]) > statusOrder(statusFlag)):
                statusFlag = result[1]
                if (statusFlag == nap.WARNING):
                    summaryMSG = "%s test with warning" % result[0]
                else:
                    summaryMSG = "%s test with error" % result[0]
        except IndexError:
            io.write("Unexepected app.metric_result")

    io.set_status(statusFlag, summaryMSG)
    return

if __name__ == '__main__':
    app.run()
