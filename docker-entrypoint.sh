#!/bin/bash

source /usr/bin/etf-init.sh

cat << "EOF"
  _____ _____ _____   _     _   _  ____ _
 | ____|_   _|  ___| | |   | | | |/ ___| |__
 |  _|   | | | |_    | |   | |_| | |   | '_ \
 | |___  | | |  _|   | |___|  _  | |___| |_) |
 |_____| |_| |_|     |_____|_| |_|\____|_.__/
==============================================
EOF

print_header

etf_update

#REMOVE ME: remove once container is updated and we have newer vo-feed package
/bin/bash -c 'cd /usr/lib/python2.7/site-packages/vofeed; patch -p1 < vo_feed_api.patch' || echo "Failed to patch vo_feed api"

start_xinetd

etf_init

copy_certs

apache_init

config_web_access

config_alerts

init_api

config_htcondor

config_etf

fix_cmk_theme

etf_start

start_oidc_agent || echo "OIDC agent start failed"

echo "Fetching LHCb credentials ..."
su etf -c "/usr/lib/nagios/plugins/globus/refresh_proxy -b 2048 --vo-fqan /lhcb/Role=production --myproxyuser nagios -H myproxy.cern.ch -t 120 --key /opt/omd/sites/etf/etc/nagios/globus/etf_srv_key.pem --vo lhcb --lifetime 24 --name NagiosRetrieve-ETF-lhcb -x /opt/omd/sites/etf/etc/nagios/globus/userproxy.pem--lhcb --cert /opt/omd/sites/etf/etc/nagios/globus/etf_srv_cert.pem"

echo "Initialising tokens ..."
su - etf -c "oidc-add --pw-file=/opt/omd/sites/etf/.oidc-agent/lhcb-etf.key lhcb-etf" || echo "Token init failed"

etf_wait
