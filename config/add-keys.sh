#!/bin/bash
source /opt/omd/sites/etf/.oidc-agent/oidc-env.sh
cp -f /etc/grid-security/tokens/* /opt/omd/sites/etf/.oidc-agent/
/usr/bin/oidc-add --pw-file=/opt/omd/sites/etf/.oidc-agent/lhcb-etf.key lhcb-etf
/usr/lib64/nagios/plugins/refresh_token -t 890 --token-time 10800 --token-config lhcb-etf -x /opt/omd/sites/etf/etc/nagios/globus/lhcb-ce.token
