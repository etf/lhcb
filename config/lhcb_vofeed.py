import re
import logging
import json
import random

from ncgx.inventory import Hosts, Checks, Groups
from vofeed.api import VOFeed

log = logging.getLogger('ncgx')

FLAVOR_MAP = {'ARC-CE-HTTPS': 'nordugrid',
              'HTCONDOR-CE': 'condor'}

WN_METRICS = {
    'WN-lhcb-env': 'org.lhcb.WN-env-/lhcb/Role=production',
    'WN-lhcb-cvmfs': 'org.lhcb.WN-cvmfs-/lhcb/Role=production',
    'WN-singularity': 'org.lhcb.WN-singularity-/lhcb/Role=production',
}

ARC_METRICS = (
    'org.sam.ARC-JobSubmit-/lhcb/Role=production',
)

CONDOR_METRICS = (
    'org.sam.CONDOR-JobSubmit-/lhcb/Role=production',
    'org.sam.CONDOR-Ping-/lhcb-ce-token',
)

WEBDAV_METRICS = (
    'org.lhcb.SE-WebDAV-connection',
    'org.lhcb.SE-WebDAV-ssl',
    'org.lhcb.SE-WebDAV-extensions',
    'org.lhcb.SE-WebDAV-write')

GRIDFTP_METRICS = (
    'org.lhcb.SE-GSIftp-connection',
    'org.lhcb.SE-GSIftp-write')

XROOTD_METRICS = (
    'org.lhcb.SE-xrootd-connection',
    'org.lhcb.SE-xrootd-write')

def run(url, ipv6=False):
    log.info("Processing vo feed: %s" % url)

    # Get services from the VO feed, i.e
    # list of tuples (hostname, flavor, endpoint)
    feed = VOFeed(url)
    services = feed.get_services(all_attrs=True)

    # Create base hosts object
    # Hosts are added below in the corresponding loops
    h = Hosts()

    # Add corresponding metrics to tags
    # creates /etc/ncgx/conf.d/generated_checks.cfg
    c = Checks()
    c.add_all(ARC_METRICS, tags=["ARC-CE-HTTPS", ])
    c.add_all(CONDOR_METRICS, tags=["HTCONDOR-CE",
                                    "org.opensciencegrid.htcondorce"])
    c.add_all(WN_METRICS.values(), tags=["ARC-CE-HTTPS", "HTCONDOR-CE",
                                         "org.opensciencegrid.htcondorce"])
    c.add("org.lhcb.DNS-IPv6", tags=["SRMv2"], params={'extends': 'check_dig'})

    # ETF env - environment variables to export on the worker node
    # ETF_TESTS pointing to a list of WN tests to execute
    with open('/tmp/etf-env.sh', 'w') as etf_env:
        etf_env.write('ETF_TESTS={}\n'.format(','.join(['etf/probes/'+m for m in WN_METRICS.keys()])))

    # ETF WN-qFM config - maps WN tests to metrics (WN-cvmfs -> org.lhcb.WN-cvmfs-/lhcb/Role=production)
    with open('/tmp/etf_wnfm.json', 'w') as etf_wnfm:
        json.dump({'wn_metric_map': WN_METRICS, 'counter_enabled': True}, etf_wnfm)

    ce_host_port_rexp = re.compile('^([^:]+):([0-9]+)$')
    # Queues
    for service in services:
        host = service[0]
        #Remove port from host name, save host+port in another variable
        ce_port = None
        m = ce_host_port_rexp.match(host)
        if m:
            host = m.group(1)
            ce_port = m.group(2)
            #Do not add default GridFTP port. It can spoil arcrest submission.
            if ce_port != '2811':
                host_full = '{0}:{1}'.format(host, ce_port)
            else:
                host_full = host
        else:
            host_full = host

        flavor = service[1]
        endpoint = service[2]

        if flavor not in FLAVOR_MAP.keys():
            continue
        queue = 'noqueue'
        # Try to get queue from the config
        ce_resources = feed.get_queues(host, flavor)
        if ce_resources:
            if len(ce_resources) > 1:
                log.warning("More than one queue found for service %s (%s) - %s" % (host, flavor, ce_resources))
                ce_res_nogpu = [e for e in ce_resources if
                                all(sp not in e[1].lower() for sp in ['gpu',
                                            'debug', 'atlas', 'cms', 'alice',
                                            'himem'])]
                if ce_res_nogpu:
                    # pick at random from remaining queues
                    ce_res = random.choice(ce_res_nogpu)
                    queue = ce_res[1]
                else:
                    log.error("Only restricted queues found for service %s (%s), skipping" % (host, flavor))
                    continue
            else:
                # just one queue option
                queue = ce_resources[0][1]

        if flavor == 'ARC-CE-HTTPS':
            # arc2.farm.particle.cz:2811/nordugrid-Condor-grid
            res = f"{FLAVOR_MAP[flavor]}://{host_full}/nosched/nopbs/{queue}"
            test_args = {'--resource': res}
            # For non-standard ports we have to explicitly specify endpoint.
            # Otherwise port from `resource` option is ignored.
            if ce_port and ce_port != '2811' and ce_port != '443':
                test_args['--arc-ce'] = host_full
            c.add('org.sam.ARC-JobState-/lhcb/Role=production', hosts=(host,),
                  params={'args': test_args})

            # Token tests for ARC -- submitted via HTCondor
            # token_endpoint = f"arc://{host_full}/nosched/nopbs/{queue}"
            # c.add('org.sam.CONDOR-JobState-/lhcb-ce-token', hosts=(host,),
            #    params={
            #        'args': {
            #            '--resource': token_endpoint,
            #            '--arc-rte': 'ENV/PROXY',
            #            '--jdl-ads': '\'maxWallTime=30\''
            #        }
            #    }
            # )
            # c.add('org.sam.CONDOR-JobSubmit-/lhcb-ce-token', hosts=(host,), params={'_tags': flavor})
        else:
            # htcondor-ce
            c.add('org.sam.CONDOR-JobState-/lhcb/Role=production',
                  hosts=(host,),
                  params={'args': {'--resource': 'condor://%s' % host}})
        h.add(host, tags=[flavor])

    # WebDAV, GRIDFTP
    pat = re.compile(r"^\w+://.+:(\d*)(/.*)")
    defport = {'HTTP': 443, 'GRIDFTP': 2811, 'XROOTD': 1094}
    for service in services:
        host = service[0]
        flavor = service[1]

        if flavor not in ['HTTP', 'GRIDFTP', 'XROOTD']:
            continue

        try:
            service_extra = dict(service[3])
            read_only = service_extra['read_only'] == "True"
            read_only = read_only or  service_extra['architecture'] == "Tape"
        except (IndexError, KeyError):
            read_only = False

        endpoint = service[2]
        args_dict = dict()
        m = re.search(pat, endpoint)
        if (m):
            port = m.group(1)
            path = m.group(2)
            if (not port):
                port = defport[flavor]
            path = re.sub(r'/$', '', path)
            if not (re.search(r'/lhcb', path)):
                path = path + '/lhcb/etf'

            args_dict['-P'] = port
            args_dict['-T'] = path
        args_dict['-H'] = host
        if ipv6:
            args_dict['-6'] = ''
        else:
            args_dict['-4'] = ''


        probe_name = None
        if flavor == 'HTTP':
            probe_name = "org.lhcb.SE-WebDAV-summary"
            flv_metrics = WEBDAV_METRICS
        elif flavor == 'GRIDFTP':
            probe_name = "org.lhcb.SE-GSIftp-summary"
            flv_metrics = GRIDFTP_METRICS
        elif flavor == 'XROOTD':
            probe_name = "org.lhcb.SE-xrootd-summary"
            flv_metrics = XROOTD_METRICS

        if probe_name is not None:
            if read_only:
                args_dict['-b'] = ''
            c.add(probe_name,
                hosts=(host,), params={'args': args_dict, '_tags': flavor})

            # For read-only SEs keep just the simplest metrics
            if read_only:
                flv_metrics = tuple(x for x in flv_metrics if x.endswith('connection'))

            for mtrc in flv_metrics:
                c.add(mtrc, hosts=(host,))
        h.add(service[0], tags=[service[1]])

    #Serialize hosts and checks
    h.serialize()
    c.serialize()

    # Add host groups
    sites = feed.get_groups("LHCb_Site")
    hg = Groups("host_groups")
    added_hosts = h.get_all_hosts()
    for site, hosts in sites.items():
        for host in hosts:
            m = ce_host_port_rexp.match(host)
            if m:
                hostname = m.group(1)
            else:
                hostname = host
            if hostname in added_hosts:
                hg.add(site, hostname)
    hg.serialize()
