# org.lhcb.SE-WebDAV-extensions
This test performs the following operations:
* Opens an SSL connection and sends an HTTP request using the PROPFIND method (used to discover server properties)
* Checks if the WebDAV extensions are supported
* Prints information from the server response

These events determine a not-OK test result:
| Condition | Test status |
|-----------|-------------|
| WebDAV extensions not implemented | CRITICAL |
| Failed to send the request via SSL | CRITICAL |
| Failed to send the PROPFIND request | CRITICAL |
| Any other error returned by the request | CRITICAL |

## Summary
The test will pass only if it is possible to send a PROPFIND request to the endpoint and the WebDAV extensions are available.
