# org.lhcb.SE-xrootd-write
This test performs the following operations:

* Opens a file for writing on the endpoint via GFAL2 using the xrootd protocol
* Gets the file checksum from GFAL2 using the xrootd protocol
* Deletes the file from the endpoint via GFAL2 using the xrootd protocol

These events determine a not-OK test result:
| Condition | Test status |
|-----------|-------------|
| Number of written bytes differs from buffer size | CRITICAL |
| Size of written file differs from buffer size | CRITICAL |
| GFAL2 produces an error when writing | CRITICAL |
| The file checksum from GFAL2 is incorrect | CRITICAL |
| The checksum was not returned | CRITICAL |
| The file could not be removed using GFAL2 | WARNING |
